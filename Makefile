
CC=g++

CFLAGS=-Wall

EXECUTABLE=radar

CFILES=radar.c serial.c out_of_box_demo.c overhead_3d_people_counting.c json.c
OFILES=radar.o serial.o out_of_box_demo.o overhead_3d_people_counting.o json.o

$(EXECUTABLE): $(OFILES)
	$(CC) $(CFLAGS) -o $(EXECUTABLE) $(OFILES) -lm

%.o:	%.c
	$(CC) $(CFLAGS) -c $< -o $@ 

clean:
	rm -f $(OFILES) $(EXECUTABLE)
