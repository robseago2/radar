#include <stdio.h>
#include <string.h>
#include <fcntl.h>  /* File Control Definitions          */
#include <unistd.h> /* UNIX Standard Definitions         */
#include <termios.h>/* POSIX Terminal Control Definitions*/
#include <errno.h>

#include "radar.h"

int open_serial(char *dev, int speed)
{
    int fd;
    int rv;
    struct termios tty;

    fd = open(dev, O_RDWR | O_NOCTTY | O_NONBLOCK); // non blocking, no terminal
    if(fd < 0) return -1;

    rv = tcgetattr(fd, &tty);
    if(rv < 0) {
        printf("tcgetattr failed with errno=%d\n", errno);
        return -1;
    }

    tty.c_cflag = (tty.c_cflag & ~CSIZE) | CS8;     // 8-bit chars
    // disable IGNBRK for mismatched speed tests; otherwise receive break
    // as \000 chars
    tty.c_iflag &= ~IGNBRK;         // disable break processing
    tty.c_lflag = 0;                // no signaling chars, no echo,
                                    // no canonical processing
    tty.c_oflag = 0;                // no remapping, no delays
    tty.c_cc[VMIN]  = 0;            // read doesn't block
    tty.c_cc[VTIME] = 5;            // 0.5 seconds read timeout

    tty.c_iflag &= ~(IXON | IXOFF | IXANY); // shut off xon/xoff ctrl

    tty.c_cflag |= (CLOCAL | CREAD);// ignore modem controls,
                                    // enable reading
    tty.c_cflag &= ~(PARENB | PARODD);      // shut off parity
    tty.c_cflag &= ~CSTOPB;

    /* o before i */
    rv = cfsetospeed(&tty, speed);
    if(rv < 0) {
        printf("cfsetospeed failed with errno=%d\n", errno);
        return -1;
    }

    rv = cfsetispeed(&tty, speed);
    if(rv < 0) {
        printf("cfsetispeed failed with errno=%d\n", errno);
        return -1;
    }

     /* apply settings with flush */
    rv = tcsetattr(fd, TCSAFLUSH, &tty);
//    rv = tcsetattr(fd, TCSANOW, &tty);
    if(rv < 0) {
        printf("tcsetattr failed with errno=%d\n", errno);
        return -1;
    }

    return fd;
}

int flush_serial(int fd)
{
    char rbuf[128];
    int red;

    red = read(fd, rbuf, 100);

//    if (red > 0)
    printf("Flush[%d] got %d bytes\n", fd, red);
    return red;
}

void close_serial(int fd)
{
    if(fd < 0) return;
    close(fd);
}