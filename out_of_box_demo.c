#include <stdio.h>
#include <stdint.h>

#include "radar.h"

#define OUTFILE "/var/www/html/radar.csv"

/* these structures are defined in mmwave_industrial_toolbox_4_4_1 */
#define MMWDEMO_OUTPUT_MSG_DETECTED_POINTS 1
#define MMWDEMO_OUTPUT_MSG_RANGE_PROFILE 2
#define MMWDEMO_OUTPUT_MSG_NOISE_PROFILE 3
#define MMWDEMO_OUTPUT_MSG_AZIMUT_STATIC_HEAT_MAP 4
#define MMWDEMO_OUTPUT_MSG_RANGE_DOPPLER_HEAT_MAP 5
#define MMWDEMO_OUTPUT_MSG_STATS 6
#define MMWDEMO_OUTPUT_MSG_DETECTED_POINTS_SIDE_INFO 7


/* 40 Byte Header */
typedef struct{
    uint32_t MagicWord0;  // technically the magicword is 4x16-bit works
    uint32_t MagicWord1;
    uint32_t version;
    uint32_t totalPacketLen;
    uint32_t platform;
    uint32_t frameNumber;
    uint32_t timeCpuCycles;
    uint32_t numDetectedObj;
    uint32_t numTLVs;
    uint32_t reserved;
}header_t;

/* 8 Byte TLV Header */
typedef struct{
    uint32_t Type;
    uint32_t Length;
}tlv_header_t;

/* point cloud */
typedef struct{
    float x;
    float y;
    float z;
    float velocity;
}point_cloud_cartesian_t;

static int discard = 0;

static int tdp = 0; // processed
static int tdd = 0; // discarded

static int process_range_profile_msg(unsigned char *buf, int len)
{
    uint16_t *pu;
    int range_fft_size;
    int range_valid;

  printf("\t%s\n", __FUNCTION__);

    range_fft_size = 256;
 
    pu = (uint16_t *) buf;
    int bin = 0;
    float sum = 0;
    float max = 0;
    uint16_t u16;
    float mag_log2;
    for(int i = 0; i < range_fft_size; i++) {

        u16 = *pu++;
        mag_log2 = ((float)u16)/512.0; // Q9 to float

        sum += mag_log2;
        if(mag_log2 > max) {
            max = mag_log2;
            bin = i;      
        }
    }

    range_valid = 1;
    if((bin <= 1) || (bin >= range_fft_size-2))
        range_valid = 0;

    printf("Bin[%d] Max=%4.2f Sum=%4.2f valid=%d\n", bin, max, sum, range_valid);

    return range_valid;

}

static int process_detected_points_msg(unsigned char *buf, int len)
{
    point_cloud_cartesian_t *pc;
  printf("\t%s\n", __FUNCTION__);

    int num_obj = (len)/sizeof(point_cloud_cartesian_t);

    FILE *fo;
    fo = fopen(OUTFILE, "w");
    if(!fo) return -1;

    fprintf(fo, "x,y,z\n");

    pc = (point_cloud_cartesian_t *)buf;
    for(int i = 0; i < num_obj; i++) {
//        printf("Obj[%d] x=%4.2f y=%4.2f z=%4.2f v=%4.2f\n", i, pc->x, pc->y, pc->z, pc->velocity);
        fprintf(fo, "%4.4f,%4.4f,%4.4f\n", pc->x, pc->y, pc->z);
        pc++;

    }
    fclose(fo);
    return num_obj;
}

static int process_stats_msg(unsigned char *buf, int len)
{
printf("\t%s\n", __FUNCTION__);

    if(len != 6*sizeof(uint32_t)) {
        printf("length of %d is invalid\n", len);
    }
    return 0;
}

int process_out_of_box_demo_frame(unsigned char *buf, int len)
{
    unsigned char *p;
    header_t *hdr;
    int psize;

    p = buf;
    hdr = (header_t *)p;

    if((hdr->MagicWord0 != 0x03040102) || (hdr->MagicWord1 != 0x07080506)) {
        printf("Discarding len=%d bytes, magic=%08x %08x\n", len, hdr->MagicWord0, hdr->MagicWord1);
        discard++;
        tdd += len;
        return len;
    }

    /* incomplete */
    psize = hdr->totalPacketLen; // packetsize
    if(psize > len) return 0;

    printf("Header version=%08x tpl=%d platform=%08x fnum=%d obj=%d tlvs=%d\n", hdr->version, hdr->totalPacketLen, hdr->platform, hdr->frameNumber, hdr->numDetectedObj, hdr->numTLVs);

//    print_buffer(buf, len);

    update_framenumber(hdr->frameNumber);

    if((psize % MMWDEMO_OUTPUT_MSG_SEGMENT_LEN) != 0) {
        printf("TPL %d is not a multpliple of %d\n", len, MMWDEMO_OUTPUT_MSG_SEGMENT_LEN);
    }

    p += sizeof(header_t);

    int processed = sizeof(header_t);
    
    for(int t = 0; t < (int)hdr->numTLVs; t++)
    {
        tlv_header_t *ptlv;

        ptlv = (tlv_header_t *)p;
        printf("\tTLV[%d] type=%d length=%d\n", t, ptlv->Type, ptlv->Length);
        p += sizeof(tlv_header_t);

        if((ptlv->Type > 100) || ((int)ptlv->Length > (psize-processed)) || ((int)ptlv->Length < 0) )
        {
            printf("Bogus data, 416 byte packet?\n");
            break;
        }

        switch(ptlv->Type) {
            case MMWDEMO_OUTPUT_MSG_DETECTED_POINTS:
                process_detected_points_msg(p, ptlv->Length);
                break;
            case MMWDEMO_OUTPUT_MSG_RANGE_PROFILE:
                process_range_profile_msg(p, ptlv->Length);
                break;
            case MMWDEMO_OUTPUT_MSG_NOISE_PROFILE:
                break;
            case MMWDEMO_OUTPUT_MSG_AZIMUT_STATIC_HEAT_MAP:
                break;
            case MMWDEMO_OUTPUT_MSG_RANGE_DOPPLER_HEAT_MAP:
                break;
            case MMWDEMO_OUTPUT_MSG_STATS:
                process_stats_msg(p, ptlv->Length);
                break;
            case MMWDEMO_OUTPUT_MSG_DETECTED_POINTS_SIDE_INFO:
                break;
            default:
                printf("TLV Type %d is unknown\n", ptlv->Type);
        }

        p += ptlv->Length;
        processed += (sizeof(tlv_header_t) + ptlv->Length);
    }

    int chunks = (processed + MMWDEMO_OUTPUT_MSG_SEGMENT_LEN-1)/MMWDEMO_OUTPUT_MSG_SEGMENT_LEN;
    int total_len = (chunks*MMWDEMO_OUTPUT_MSG_SEGMENT_LEN);

    tdp += psize;

//    printf("sizein=%d processed=%d valid=%d tdr=%d tdp=%d tdd=%d n1=%d)\n", psize, total_len, verify_len, tdr, tdp, tdd, n1);

    if(psize != total_len) {
        printf("**framing error! (tdr=%d)\n", tdr);
    }

    return psize;
}

