#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <math.h>

#include "radar.h"


static uint32_t FrameNumber = 0;

/* these structures are defined in mmwave_industrial_toolbox_4_4_1 */
/* see overhead_3d_people_counting_user_guide.pdf fro description */
/* see pcount3D_output.h for definitions */

#define MMWDEMO_OUTPUT_MSG_DETECTED_POINTS 1
#define MMWDEMO_OUTPUT_MSG_RANGE_PROFILE 2
#define MMWDEMO_OUTPUT_MSG_NOISE_PROFILE 3
#define MMWDEMO_OUTPUT_MSG_AZIMUT_STATIC_HEAT_MAP 4
#define MMWDEMO_OUTPUT_MSG_RANGE_DOPPLER_HEAT_MAP 5
#define MMWDEMO_OUTPUT_MSG_POINT_CLOUD 6
#define MMWDEMO_OUTPUT_MSG_TARGET_LIST 7
#define MMWDEMO_OUTPUT_MSG_TARGET_INDEX 8
#define MMWDEMO_OUTPUT_MSG_CLASSIFIER_OUTPUT 9
#define MMWDEMO_OUTPUT_MSG_STATS 10
#define MMWDEMO_OUTPUT_MSG_MAX_TLV 10

/* 48 Byte Header */
typedef struct{
    uint32_t MagicWord0;  // technically the magicword is 4x16-bit works
    uint32_t MagicWord1;
    uint32_t version;
    uint32_t totalPacketLen;
    uint32_t platform;
    uint32_t frameNumber;

    /* end of common 24-byte header? */

    /* additional 24-bytes */

    /*! @brief   For Advanced Frame config, this is the sub-frame number in the range
     * 0 to (number of subframes - 1). For frame config (not advanced), this is always
     * set to 0. */
    uint32_t subFrameNumber;

    /*! @brief Detection Layer timing */
    uint32_t chirpProcessingMargin;
    uint32_t frameProcessingTimeInUsec;

    /*! @brief Localization Layer Timing */
    uint32_t trackingProcessingTimeInUsec;
    uint32_t uartSendingTimeInUsec;


    /*! @brief   Number of TLVs */
    uint16_t numTLVs;

    /*! @brief   check sum of the header */
    uint16_t checkSum;

}header_t;

/* 8 Byte TLV Header */
typedef struct{
    uint32_t Type;
    uint32_t Length;
}tlv_header_t;

typedef struct
{
    uint32_t interFrameProcessingTime;  /* Interframe processing time in usec */
    uint32_t transmitOutputTime;  /* Transmission time of output detection information in usec */
    uint32_t frameProcessingTimeInUsec; /* Interframe processing margin in usec */
    uint32_t interChirpProcessingMargin; /* Interchirp processing margin in usec */    
    uint32_t activeFrameCPULoad; /* CPU Load (%) during active frame duration */
    uint32_t interFrameCPULoad; /* CPU Load (%) during inter frame duration */
} stats_t;

typedef struct  // *** scale 
{
    float elevation; /*! @brief elevation  reporting unit, in radians */
    float azimuth; /*! @brief azimuth  reporting unit, in radians */
    float doppler; /*! @brief Doppler  reporting unit, in m/s */
    float range; /*! @brief range reporting unit, in m */
    float snr; /*! @brief SNR  reporting unit, linear */
} point_unit_t;

typedef struct
{
    int8_t elevation;  /*! @brief Detected point elevation, in number of azimuthUnit */
    int8_t azimuth;  /*! @brief Detected point azimuth, in number of azimuthUnit */
    int16_t doppler; /*! @brief Detected point doppler, in number of dopplerUnit */
    uint16_t range; /*! @brief Detected point range, in number of rangeUnit */
    uint16_t snr;  /*! @brief Range detection SNR, in number of snrUnit */
} point_struct_t;

typedef struct
{
    uint32_t tid; // Track ID
    float posX; // Target position in X dimension, m
    float posY; // Target position in Y dimension, m
    float posZ; // Target position in Z dimension, m
    float velX; // Target velocity in X dimension, m/s
    float velY; // Target velocity in Y dimension, m/s
    float velZ; // Target velocity in Z dimension, m/s
    float accX; // Target acceleration in X dimension, m/s2
    float accY; // Target acceleration in Y dimension, m/s
    float accZ; // Target acceleration in Z dimension, m/s
    float ec[16]; // Tracking error covariance matrix, [4x4] in range/azimuth/elevation/doppler coordinates
    float g; // Gating function gain
    float confidenceLevel; // Confidence Level
} target_t;

#define MAX_TRACKER 256
#define MAX_POINTS 50 // 5 seconds at 10fps
#define TRACKER_RETAIN 25

typedef struct {
    float x;
    float y;
    float z;
} point_t;

typedef struct {
    int index;
    int count;
    int active;

    uint32_t fnum;

    point_t point[MAX_POINTS];

}tracker_t;

static tracker_t Tracker[MAX_TRACKER];
static int MaxId = 0;

static void update_tracker(uint32_t fnum, int tid, float x, float y, float z)
{
    tracker_t *t;
    point_t *p;

    if(tid >= MAX_TRACKER) return;
    
    t = &Tracker[tid];
    if(t->active == 0) {
        t->index = 0;
        t->count = 0;
        t->active = 1;
        printf("TrackerAdd[%d]\n", tid);
    }
    p = &t->point[t->index];
    p->x = x;
    p->y = y;
    p->z = z;

    t->fnum = fnum;
    t->index++;
    if(t->index >= MAX_POINTS)
        t->index = 0;
    if(t->count < MAX_POINTS)
        t->count++;

    if (tid > MaxId)
        MaxId = tid;
}

static void output_tracker(uint32_t fnum)
{
    tracker_t *t;
    point_t *p;
    int si;
    int appendobj = 0;
    int appendpos = 0;
    int objectcount = 0;
    char str[256];

    sprintf(str, "\"targets\": [\n");
    JSON_ADD_STR(str);
    for (int i = 0; i <= MaxId; i++) {
        t = &Tracker[i];

        appendpos = 0;
        if(t->active) {

            if(t->fnum + TRACKER_RETAIN > fnum) {
                if(appendobj) {
                    sprintf(str, ",\n");
                    JSON_ADD_STR(str);
                }
                sprintf(str, "{ \"id\": %d, \"pos\": [", i);
                JSON_ADD_STR(str);
                si = (t->count < MAX_POINTS) ? 0 : t->index;
                for(int z = 0; z < t->count; z++) {
                    int ii = si + z;

                    if(ii >= MAX_POINTS) ii -= MAX_POINTS;
                    p = &t->point[ii];
                    if(appendpos) {
                        sprintf(str, ",");
                        JSON_ADD_STR(str);
                    }
                    sprintf(str, "[%4.3f,%4.3f,%4.3f]", p->x, p->y, p->z);
                    JSON_ADD_STR(str);
                    appendpos = 1;

                }
                sprintf(str, "] }");
                JSON_ADD_STR(str);
                appendobj = 1;
                objectcount++;

            }
            else { // kill it
                t->active = 0;
                printf("TrackerRemove[%d]\n", i);

            }
        }
    }
    sprintf(str, "\n],\n");
    JSON_ADD_STR(str);

    sprintf(str, "\"objectCount\": %d\n", objectcount);
    JSON_ADD_STR(str);

    /* Reset MaxId when list is empty */
    if(objectcount == 0)
        MaxId = 0;
}


static int discard = 0;

static int tdp = 0; // processed
static int tdd = 0; // discarded

typedef struct{
    float x;
    float y;
}coord_t;


static coord_t *coord_list;
static int coord_count;

static void alloc_coord_list(int num)
{
    coord_list = (coord_t *)malloc(num*sizeof(coord_t));
    coord_count = 0;
}

static void add_to_coord_list(float x, float y)
{
    (coord_list+coord_count)->x = x;
    (coord_list+coord_count)->y = y;
    coord_count++;
}

static void free_coord_list(void)
{
    free(coord_list);
}


static int process_point_cloud_msg(unsigned char *buf, int len)
{
    uint8_t *pu8;
    point_unit_t *pu;
    point_struct_t *ps;
    int appendpos;
    char str[256];

    int num_obj = (len - sizeof(point_unit_t))/sizeof(point_struct_t);

    if(num_obj > 100){
        printf("num_obj %d is invalid, data is corrupt\n", num_obj);
        return 0;
    }

    alloc_coord_list(num_obj);

    sprintf(str, "\"cloud\": [\n");
    JSON_ADD_STR(str);

pu8 = buf;
pu = (point_unit_t *)pu8;
pu8 += sizeof(point_unit_t);
ps = (point_struct_t *)pu8;

    printf("num_obj=%d Unit el=%4.4f az=%4.4f dop=%4.4f rng=%4.4f snr=%4.4f\n", num_obj, pu->elevation, pu->azimuth, pu->doppler, pu->range, pu->snr);

    appendpos = 0;
    for(int i = 0; i < num_obj; i++) {
        point_unit_t obj, *po;

        po = &obj;
        po->elevation = pu->elevation * (float)ps->elevation;
        po->azimuth = pu->azimuth * (float)ps->azimuth;
        po->doppler = pu->doppler * (float)ps->doppler;
        po->range = pu->range * (float)ps->range;
        po->snr = pu->snr * (float)ps->snr;

        float yy = po->range * sin(po->elevation);
        float xx = po->range * sin(po->azimuth);

    printf("Pnt[%d] el=%4.4f az=%4.4f dop=%4.4f rng=%4.4f snr=%4.4f xx=%4.4f yy=%4.4f\n", i, po->elevation, po->azimuth, po->doppler, po->range, po->snr, xx, yy);

        add_to_coord_list(xx, yy);

        if(appendpos) {
            sprintf(str, ",");
            JSON_ADD_STR(str);
        }

        sprintf(str, "[%4.3f,%4.3f,%4.3f]", xx, po->range, yy);
        JSON_ADD_STR(str);
        appendpos = 1;

        ps++;

    }

    sprintf(str, "\n],\n");
    JSON_ADD_STR(str);

//    int num_targets = process_cloud(coord_list, coord_count);

    free_coord_list();



    return num_obj;
}

static int process_target_list_msg(unsigned char *buf, int len)
{
    target_t *pt;
    int num_targets = len/sizeof(target_t);

    pt = (target_t *)buf;
    for(int i = 0; i < num_targets; i++) {
        int tid = pt->tid;
        /* validate */
        if((tid < 0) || (tid > 255)) {
            printf("TID=%d in invalid, data is corrupt\n", tid);
            break;
        }
        if((fabs(pt->posX) > 10) || (fabs(pt->posY) > 10) || (fabs(pt->posZ) > 10)) {
            printf("Position (x=%4.2f y=%4.2f z=%4.2f) is invalid, data is corrupt\n", pt->posX, pt->posY, pt->posZ);
            break;
        }
        if((pt->confidenceLevel < 0.1) || (pt->confidenceLevel > 1.0)) {
            printf("Confidence %4.2f is invalid, data is corrupt\n", pt->confidenceLevel);
            break;
        }

        printf("Tgt[%d] TID=%d x=%4.2f y=%4.2f z=%4.2f conf=%4.2f\n", i, pt->tid, pt->posX, pt->posY, pt->posZ, pt->confidenceLevel);

        update_tracker(FrameNumber, pt->tid, pt->posX, pt->posY, pt->posZ);
        pt++;
    }
    return num_targets;
}

static int process_target_index_msg(unsigned char *buf, int len)
{
    uint8_t *pu8;

    int num_obj = len;  // in the previous frame
    pu8 = buf;
    for(int i = 0; i < num_obj; i++) {
        printf("Obj[%d] TID=%d\n", i, *pu8);
//        fprintf(fo, "%4.4f,%4.4f,%4.4f\n", pc->x, pc->y, pc->z);
        pu8++;
    }
    return num_obj;
}

static int process_stats_msg(unsigned char *buf, int len)
{
printf("\t%s\n", __FUNCTION__);

    if(len != sizeof(stats_t)) {
        printf("length of %d is invalid\n", len);
    }
    return 0;
}

int process_overhead_3d_people_counting_frame(unsigned char *buf, int len)
{
    unsigned char *p;
    header_t *hdr;
    int psize;
    int processed;
    char str[256];

    p = buf;
    hdr = (header_t *)p;

    /* need at least a full header to start processing */
    if(len < (int)sizeof(header_t))
        return 0;

    if((hdr->MagicWord0 != 0x03040102) || (hdr->MagicWord1 != 0x07080506)) {
        printf("Discarding len=%d bytes, magic=%08x %08x\n", len, hdr->MagicWord0, hdr->MagicWord1);
        discard++;
        tdd += len;
        return len;
    }

    /* incomplete */
    psize = hdr->totalPacketLen; // packetsize
    if(psize > len) return 0;

    printf("Header version=%08x tpl=%d platform=%08x fnum=%d sfnum=%d tlvs=%d\n", hdr->version, hdr->totalPacketLen, hdr->platform, hdr->frameNumber, hdr->subFrameNumber, hdr->numTLVs);

//    update_framenumber(hdr->frameNumber);
FrameNumber = hdr->frameNumber;

//    print_buffer(buf, len);

    /* packet sizes on this firmware are not padded to 32 bytes */

//    if((psize % MMWDEMO_OUTPUT_MSG_SEGMENT_LEN) != 0) {
//        printf("TPL %d is not a multpliple of %d\n", len, MMWDEMO_OUTPUT_MSG_SEGMENT_LEN);
//    }

    /* validate packet first before processing  */
    p += sizeof(header_t);
    processed = sizeof(header_t);
    
    for(int t = 0; t < (int)hdr->numTLVs; t++)
    {
        tlv_header_t *ptlv;

        ptlv = (tlv_header_t *)p;
        printf("\tTLV[%d] type=%d length=%d processed=%d\n", t, ptlv->Type, ptlv->Length, processed);

        if((ptlv->Type > MMWDEMO_OUTPUT_MSG_MAX_TLV) || ((int)ptlv->Length > (psize-processed)) || ((int)ptlv->Length < 0) )
        {
            printf("Bogus data, 416 byte packet?\n");
            return psize;
        }

        p += ptlv->Length; // header + payload
        processed += ptlv->Length;
    }
    /* occasssionaly get an extra 3 bytes??? */
    if(psize != processed) {
        printf("**framing error! extra=%d (tdr=%d) \n", psize-processed, tdr);
    }

    JSON_CLEAR();

    sprintf(str, "{\n");
    JSON_ADD_STR(str);

    sprintf(str, "\"frameNumber\": %d,\n", (int) FrameNumber);
    JSON_ADD_STR(str);


    p = buf;
    p += sizeof(header_t);

    processed = sizeof(header_t);
    
    for(int t = 0; t < (int)hdr->numTLVs; t++)
    {
        tlv_header_t *ptlv;

        ptlv = (tlv_header_t *)p;
        printf("\tTLV[%d] type=%d length=%d processed=%d\n", t, ptlv->Type, ptlv->Length, processed);
//        p += sizeof(tlv_header_t);

        if((ptlv->Type > 100) || ((int)ptlv->Length > (psize-processed)) || ((int)ptlv->Length < 0) )
        {
            printf("Bogus data, 416 byte packet?\n");
            break;
        }

        unsigned char *payload;
        payload = p + sizeof(tlv_header_t); // skip past the header
        int plen = ptlv->Length - sizeof(tlv_header_t);  // Length includes tlv header in this firmware

        switch(ptlv->Type) {

            case MMWDEMO_OUTPUT_MSG_DETECTED_POINTS:
            case MMWDEMO_OUTPUT_MSG_RANGE_PROFILE:
            case MMWDEMO_OUTPUT_MSG_NOISE_PROFILE:
            case MMWDEMO_OUTPUT_MSG_AZIMUT_STATIC_HEAT_MAP:
            case MMWDEMO_OUTPUT_MSG_RANGE_DOPPLER_HEAT_MAP:
            case MMWDEMO_OUTPUT_MSG_CLASSIFIER_OUTPUT:
                break;

            case MMWDEMO_OUTPUT_MSG_TARGET_LIST:
                process_target_list_msg(payload, plen);
                break;
            case MMWDEMO_OUTPUT_MSG_TARGET_INDEX:
                process_target_index_msg(payload, plen);
                break;
            case MMWDEMO_OUTPUT_MSG_POINT_CLOUD:
                process_point_cloud_msg(payload, plen);
                break;
            case MMWDEMO_OUTPUT_MSG_STATS:
                process_stats_msg(payload, plen);
                break;
            default:
                printf("TLV Type %d is unknown\n", ptlv->Type);
        }

        p += ptlv->Length; // header + payload
        processed += ptlv->Length;
    }

    tdp += psize;

    printf("psize=%d processed=%d tdr=%d tdp=%d)\n", psize, processed, tdr, tdp);

    output_tracker(FrameNumber);

    sprintf(str, "}\n");
    JSON_ADD_STR(str);

    JSON_OUT();

    return psize;
}
