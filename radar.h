#include <inttypes.h>

#define MMWDEMO_OUTPUT_MSG_SEGMENT_LEN 32 // packets are padded up to the next 32 byte boundary

/* serial.c */
extern int open_serial(char *dev, int speed);
extern int flush_serial(int fd);
extern void close_serial(int fd);

extern int process_packet(unsigned char *buf, int len);
extern void print_buffer(unsigned char *buf, int len);

extern void update_framenumber(uint32_t fnum);

/* processing functions */
extern int process_out_of_box_demo_frame(unsigned char *buf, int len);
extern int process_overhead_3d_people_counting_frame(unsigned char *buf, int len);

extern int tdr;  // the windows serial driver from TI is buggy, this is not needed, but shows when data has been dropped 


/* simple JSON utils */
extern void JSON_CLEAR(void);
extern void JSON_OUT(void);
extern void JSON_ADD_STR(char *s);

