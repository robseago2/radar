
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include <fcntl.h>  /* File Control Definitions          */
#include <unistd.h> /* UNIX Standard Definitions         */
#include <termios.h>/* POSIX Terminal Control Definitions*/
#include <sys/select.h>
#include <sys/ioctl.h>
#include <errno.h>  /* ERROR Number Definitions          */
#include <math.h>
#include <time.h>

#include "radar.h"

#define FRAMENUMBER_FILE "/var/www/html/framenumber.txt"
#define STOP_CMD "sensorStop\n"

#define SHOW_PACKET 0  // debug only

#define MODE_OUT_OF_BOX_DEMO 1
#define MODE_OVERHEAD_3D_PEOPLE_COUNTING 2

static int Mode;

//#if SHOW_PACKET
void print_buffer(unsigned char *buf, int len)
{

    int ofs;
    unsigned char *p0;

    ofs = 0;
    while(len > 0) {
        p0 = buf+ofs;
        printf("%04d: %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x\n", 
            ofs,
            *(p0+0), *(p0+1), *(p0+2), *(p0+3), *(p0+4), *(p0+5), *(p0+6), *(p0+7),
            *(p0+8), *(p0+9), *(p0+10), *(p0+11), *(p0+12), *(p0+13), *(p0+14), *(p0+15));
        len -= 16;
        ofs += 16;
    }
}
//#endif  // SHOW_PACKET

int tdr = 0; // read
static int n0 = 0;
static int n1 = 0;
char RxBuf0[1024];
unsigned char RxBuf1[10000];

/* function pointer to the correct processing function */
int (*process_frame)(unsigned char *p, int len);

static int read_aux_data(int fd)
{
    int rd;
    int processed;
    unsigned char *p0;
    unsigned char *pin;
    unsigned char *pout;

    p0 = &RxBuf1[0];
    pin = &RxBuf1[n1];
    rd = read(fd, pin, sizeof(RxBuf1) - n1);
    if(rd <= 0) {
        printf("Read returned %d\n", rd);
        return -1;
    }
    tdr += rd;
//    printf("Got %d bytes from aux\n", rd);

    n1 = n1 + rd; // total unprocessed bytes in buffer

    pout = p0; // start of buffer

Next:    
    processed = (*process_frame)(pout, n1);
//    printf("pro=%d p0=%p p=%p n1=%d\n", processed, p0, p, n1);
    if(processed == 0) {
       if(pout != p0) {
          printf("memmove %d\n", n1);
          memmove(p0, pout, n1);
       }
       return n1; // remainder
    }
    /* skip processed packet */ 
    else {
       pout += processed;
       n1 -= processed;
    }
    if(n1 > 0) {
        printf("Process next n1=%d\n", n1);
        goto Next;
    }
    return 0; // nothing left
}

static int read_ctrl_data(int fd, char *cmd)
{
    int rd;
    char *p;

    p = &RxBuf0[n0];
    rd = read(fd, p, sizeof(RxBuf0) - n0);
    RxBuf0[rd] = 0x00;

    if(rd > 0) {
//        printf("Got %d bytes from ctrl [%s]\n", rd, p);
/* response contains the prompt, don't print a newline  */
        printf("%s", p);
        if(!cmd) return 0; // from select
        if(strncmp(p, cmd, strlen(cmd)) != 0) {
            printf("Splat. %s not returned. Retry\n", cmd);
            return -1;
        }
    }
    else {
        printf("Splat. No response for %s\n", cmd);
        if(!cmd) return 0; // from select
        if(strcmp(cmd, "sensorStop\n") == 0) {
            printf("Ignoring no sensorStop response!");
            return 0;
        }

        return -1;    
    }
    n0 = 0;
    return 0;
}

static int send_configuration_to_device(int fd_uart, char *filename)
{
    FILE *fp;
    size_t len = 0;
    char *line = NULL;
    int num;
    int retry;
    int rv = 0;
    int n; // number written

    fp = fopen(filename, "r");
    if(!fp) {
        printf("Config file %s not found!\n", filename);
        return -1;
    }

    while ((num = getline(&line, &len, fp)) != -1) {
        len = num;
        retry = 0;
Retry:
        n = write(fd_uart, line, num);
        if(n != num) {
            printf("Write to UART failed n=%d instead of %d\n", n, num);
        }
        /* make sure complete response has been written */
        usleep(100000);
        if (read_ctrl_data(fd_uart, line) < 0) {
            if(++retry < 5) {
                usleep(10000); 
                goto Retry;
            }
            else {
                printf("Failed %d times\n", retry);
                rv = -1;
                break;
            }
        }
        printf("\n");  // extra line at the end
    }

    fclose(fp);
    if(line)
        free(line);

    return rv;
}

void update_framenumber(uint32_t fnum)
{
    FILE *fp = fopen(FRAMENUMBER_FILE, "w");
    if(!fp) return;

    fprintf(fp, "%u\n", fnum);
    fclose(fp);
}

static void main_loop(int user_fd, int data_fd)
{
    int nfds;
    fd_set readfds;
    struct timeval timeout;
    int rv;
    int nrd = 0;

    nfds = (data_fd > user_fd) ? data_fd : user_fd;
    printf("Setting up select %d %d %d...\n", user_fd, data_fd, nfds);

    while(1) {

Again:

        FD_ZERO(&readfds);
        FD_SET(user_fd, &readfds);
        FD_SET(data_fd, &readfds);

        /* 1 sec housekeeping timeout */
        timeout.tv_sec = 1;
        timeout.tv_usec = 0;

        rv = select(nfds+1, &readfds, NULL, NULL, &timeout);
        if(rv == 0) {
            if(errno == EINTR) {
                printf("Timeout\n");
                goto Again;
            }
        }
        else if (rv < 0) {
            printf("Select error\n");
            break;
        }
        else {
            if(FD_ISSET(user_fd, &readfds)) {
                printf("Got async data from fd0\n");
                read_ctrl_data(user_fd, NULL);
            }
            if(FD_ISSET(data_fd, &readfds)) {
                rv = read_aux_data(data_fd);
                if(rv < 0) {
                    printf("No read data\n");
                    nrd++;
                }
                printf("\n");             
            }
        }
    }
}

static void usage(void)
{
    printf("\nUsage: radar <mode>\n");
    printf("where mode:\n");
    printf("\t%d\tout of the box demo\n", MODE_OUT_OF_BOX_DEMO);
    printf("\t%d\toverhead 3d people counting\n", MODE_OVERHEAD_3D_PEOPLE_COUNTING);
    printf("\nThe correct firmware needs to be running on the radar device for the selected mode\n\n");
}

static int UserFd = -1;
static int DataFd = -1;

int main(int argc, char *argv[])
{
    const char *cfg_file;
    char *user_dev;
    char *data_dev;
    int rv = -1;
    int flushed;

    /* for testing, we can have one addition arg */
    Mode = MODE_OVERHEAD_3D_PEOPLE_COUNTING; // default
    if(argc == 2) {
        Mode = atoi(argv[1]);
    }

    switch(Mode) {
        case MODE_OUT_OF_BOX_DEMO:
            printf("Out of the box demo...\n");
            cfg_file = "cfg/out_of_box_demo.cfg";
            process_frame = &process_out_of_box_demo_frame;
            break;
        case MODE_OVERHEAD_3D_PEOPLE_COUNTING:
            printf("Overhead 3d people counting...\n");
            cfg_file = "cfg/3d_aop_overhead_3m_radial.cfg";
            process_frame = &process_overhead_3d_people_counting_frame;
        break;
        default:
            usage();
            return -1;
    }

    /* open the command port */
    user_dev = (char *)"/dev/ttyUSB0";
    UserFd = open_serial(user_dev, B115200);
    if(UserFd < 0) {
        printf("Failed to open %s (%s)\n", user_dev, strerror(errno));
        goto Done;
    }
    else {
        printf("Opened %s with fd=%d\n", user_dev, UserFd);
    }

    /* open the data port */
    data_dev = (char *)"/dev/ttyUSB1";
    DataFd = open_serial(data_dev, B921600);
    if(DataFd < 0) {
        printf("Failed to open %s (%s)\n", data_dev, strerror(errno));
        goto Done;
    }
    else {
        printf("Opened %s with fd=%d\n", data_dev, DataFd);
    }

    /* stop */

//    int n;
    char cmd[100];

    strcpy(cmd, STOP_CMD);
    write(UserFd, cmd, strlen(cmd));
    printf("Stopping...\n");
    sleep(2);

    read_ctrl_data(UserFd, NULL);
    n0 = 0;

    printf("Flushing...\n");
    do {
        flushed = flush_serial(UserFd);
        printf("Flush[%d] Got %d\n", UserFd, flushed);
    } while (flushed > 0);

    do {
        flushed = flush_serial(DataFd);
        printf("Flush[%d] Got %d\n", DataFd, flushed);
    } while (flushed > 0);

    /* */
    if(send_configuration_to_device(UserFd, (char *)cfg_file) < 0) {
        printf("Configuraion failed!\n");
        goto Done;
    }

    rv = 0;
    main_loop(UserFd, DataFd);

Done:
    close_serial(UserFd);
    close_serial(DataFd);

    return rv;
}
