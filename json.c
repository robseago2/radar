#include <stdio.h>
#include <string.h>

#include "radar.h"


#define OUTFILE "/var/www/html/radar_data.json"

static char jstr[2048000];
static int jind = 0;

void JSON_CLEAR(void)
{
    jind = 0;
    jstr[0] = 0x00;
}

void JSON_OUT(void)
{
//    printf("%s\n", jstr);
printf("JIND:%d\n", jind);

    FILE *fo;
    fo = fopen(OUTFILE, "w");
    if(!fo) return;

    fprintf(fo, "%s", jstr);
    fclose(fo);
}

void JSON_ADD_STR(char *s)
{
    int len;

    len = strlen(s);
    strcat(jstr, s);
    jind += len;
}
